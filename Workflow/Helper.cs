﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using Tridion.ContentManager;
using Tridion.Logging;

namespace TDS17.Workflow
{
    public static class Helper
    {
        public static class Config
        {
            public static bool Debug
            {
                get
                {
                    return GetConfigValue("Debug", false);
                }
            }

            public static string ReviewerGroupTitle
            {
                get
                {
                    return GetConfigValue("ReviewerGroupTitle", "Chief Editor");
                }
            }

            public static string StagingTargetTypeTitle
            {
                get
                {
                    return GetConfigValue("StagingTargetTypeTitle", "staging");
                }
            }

            public static string LiveTargetTypeTitle
            {
                get
                {
                    return GetConfigValue("LiveTargetTypeTitle", "Live");
                }
            }

            public static int SuspendAfterPublishToLive // in minutes ...
            {
                get
                {
                    return GetConfigValue("SuspendAfterPublishToLive", 0);
                }
            }

            private static XmlDocument config;
            private static XmlDocument GetConfigFromXmlFile()
            {
                if (config != null) return config;
                XmlDocument confDoc = new XmlDocument();
                string confPath = Path.Combine(Environment.GetEnvironmentVariable("TRIDION_HOME"), "config", "TDS17.conf");
                try
                {
                    confDoc.Load(confPath);
                }
                catch (Exception ex)
                {
                    Log.Error(String.Format("Could not load XmlDocument from file (file: {0})", confPath), ex, "Helper:Config:GetConfigFromXmlFile");
                }
                config = confDoc;
                return confDoc;
            }

            private static string GetConfigValue(string element, string defaultValue = "")
            {
                var componentId = "Helper:Config:GetConfigValue";
                string val = defaultValue;
                var doc = GetConfigFromXmlFile();
                if (doc != null)
                {
                    try
                    {
                        val = doc.GetElementsByTagName(element).Item(0).InnerText;
                    }
                    catch
                    {
                        Log.Warning(String.Format("Could not retrieve config value as String (element: '{0}')", element), componentId);
                    }
                }
                return val;
            }

            private static List<string> GetConfigValues(string element)
            {
                var componentId = "Helper:Config:GetConfigValues";
                List<string> val = new List<string>();
                var doc = GetConfigFromXmlFile();
                if (doc != null)
                {
                    try
                    {
                        //val = doc.SelectNodes(string.Format("//{0}/*", element)).Cast<XmlNode>().Select(x => x.InnerText).ToList();
                        val = doc.GetElementsByTagName(element).Cast<XmlNode>().Select(x => x.InnerText).ToList();
                    }
                    catch
                    {
                        Log.Warning(String.Format("Could not retrieve config value as List<string> (element: '{0}')", element), componentId);
                    }
                }
                return val;
            }

            private static bool GetConfigValue(string element, bool defaultValue = false)
            {
                var val = defaultValue;
                if (!Boolean.TryParse(GetConfigValue(element, defaultValue.ToString()), out val))
                {
                    val = defaultValue;
                }
                return val;
            }

            private static int GetConfigValue(string element, int defaultValue = 0)
            {
                var val = defaultValue;
                if (!int.TryParse(GetConfigValue(element, defaultValue.ToString()), out val))
                {
                    val = defaultValue;
                }
                return val;
            }
        }

        public static class Log
        {
            private const string _componentId = "Workflow";

            public static void Debug(string message, string componentId = _componentId, LogCategory logCategory = LogCategory.Workflow)
            {
                message = string.Format("({0})\n{1}", "DEBUG", message);
                Info(message, componentId, true, logCategory);
            }

            public static void Info(string message, string componentId = _componentId, bool onlyInDebugMode = false, LogCategory logCategory = LogCategory.Workflow)
            {
                if ((!onlyInDebugMode) || (onlyInDebugMode && Config.Debug))
                {
                    Logger.Write(String.Format("{0}:\n{1}\n\n", componentId, message), componentId, logCategory, TraceEventType.Information);
                }
            }

            public static void Warning(string message, string componentId = _componentId, LogCategory logCategory = LogCategory.Workflow)
            {
                Logger.Write(String.Format("{0}:\n{1}\n\n", componentId, message), componentId, logCategory, TraceEventType.Warning);
            }

            public static void Warning(string message, Exception ex, string componentId = _componentId, LogCategory logCategory = LogCategory.Workflow)
            {
                Logger.Write(
                    string.Format("{4}:\n{3}  \n\nMessage: {0}\nSource: {1}\nStack Trace: {2}\n\n",
                        ex.Message, ex.Source, ex.StackTrace, message, componentId
                    ),
                        componentId, logCategory, TraceEventType.Warning
                );
            }

            public static void Error(string message, string componentId = _componentId, LogCategory logCategory = LogCategory.Workflow)
            {
                Logger.Write(String.Format("{0}:\n{1}\n\n", componentId, message), componentId, logCategory, TraceEventType.Error);
            }

            public static void Error(string message, Exception ex, string componentId = _componentId, LogCategory logCategory = LogCategory.Workflow)
            {
                Logger.Write(
                    string.Format("{4}:\n{3}  \n\nMessage: {0}\nSource: {1}\nStack Trace: {2}\n\n",
                        ex.Message, ex.Source, ex.StackTrace, message, componentId
                    ),
                        componentId, logCategory, TraceEventType.Error
                );
            }

        }

        public static string GetFullMethodName(System.Reflection.MethodBase method)
        {
            return string.Format("{0}.{1}({2})", method.ReflectedType.FullName, method.Name, string.Join(",", method.GetParameters().Select(o => string.Format("{0} {1}", o.ParameterType, o.Name)).ToArray()));
        }

    }
}
