﻿namespace TDS17.Workflow.Activities
{
    class AutoFinish : BaseActivity
    {
        private string Message = "Activity automatically finished by expiration script of Activity '{0}'";

        protected override void Initialize()
        {
            FinishMessage = string.Format(Message, ActivityInstance.Title);
            base.Initialize();
        }

        protected override void Execute()
        {
            Message = "Activity automatically finished by Automated Activity '{0}'";
            Initialize();
            FinishActivity();
            Helper.Log.Debug(string.Format("{0}\n\n{1}", FinishMessage, GetActivityDetails()), Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
        }

        protected override void Expire()
        {
            Initialize();
            FinishActivity();
            Helper.Log.Debug(string.Format("{0}\n\n{1}", FinishMessage, GetActivityDetails()), Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
        }
    }
}
