﻿using System;

namespace TDS17.Workflow.Activities
{
    class Reject : BaseActivity
    {
        private const string Message = "Rejected by Automated Activity '{0}'";

        protected override void Initialize()
        {
            base.Initialize();
            FinishMessage = string.Format(Message, ActivityInstance.Title);
        }

        protected override void Execute()
        {
            Initialize();
            MarkWorkflowResult(TranslationWorkflowResults.Rejected, false);
            /* 
             * this workflow will be terminated by the EventSystem (!)
             * EventSystem will also take care of republishing to Staging
             */
            //Suspend();
            FinishActivity();
            Helper.Log.Debug(string.Format("{0}\n\n{1}", FinishMessage, GetActivityDetails()), Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
        }

        private void Suspend()
        {
            CoreServiceClient.SuspendActivity(ActivityInstance.Id, "Temporary suspension", DateTime.Now.AddSeconds(10), "TempBookmark", ReadOptions);
        }

        protected override void Resume(string bookmark = "")
        {
            FinishActivity();
            Helper.Log.Debug(string.Format("{0}\n\n{1}", FinishMessage, GetActivityDetails()), Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
        }

    }
}
