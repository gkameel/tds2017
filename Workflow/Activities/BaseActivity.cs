﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tridion.ContentManager;
using Tridion.ContentManager.CoreService.Client;
using Tridion.ContentManager.CoreService.Workflow;
using ItemType = Tridion.ContentManager.ItemType;

namespace TDS17.Workflow.Activities
{
    public abstract class BaseActivity : ExternalActivity
    {
        private const string WorkflowResultContextKey = "WorkflowResult";
        private const string componentId = "TDS17.Workflow.Activities.BaseActivity";

        public IEnumerable<WorkItemData> WorkItems
        {
            get { return ActivityInstance.WorkItems.ToList(); }
        }
        public VirtualFolderData Bundle { get; set; }
        public TrusteeData Assignee { get; set; }
        public TrusteeData NextAssignee { get; set; }
        public string FinishMessage { get; set; }
        public PublicationData Publication { get; set; }
        public ProcessDefinitionData ProcessDefinition { get; set; }
        public ActivityInstanceData SuspendedActivity { get; set; }
        public TridionActivityDefinitionData ActivityDefinition { get; set; }
        public ReadOptions ReadOptions { get; set; }
        public bool IsSuspended
        {
            get
            {
                return SuspendedActivity != null && SuspendedActivity.ActivityState == ActivityState.Suspended;
            }
        }

        private bool isInitialized = false;
        protected virtual void Initialize()
        {
            ReadOptions = new ReadOptions();
            Bundle = GetBundle();
            Publication = GetPublication();
            ActivityDefinition = (TridionActivityDefinitionData)CoreServiceClient.Read(ActivityInstance.ActivityDefinition.IdRef, ReadOptions);
            ProcessDefinition = (ProcessDefinitionData)CoreServiceClient.Read(ProcessInstance.ProcessDefinition.IdRef, ReadOptions);
            Assignee = (TrusteeData)CoreServiceClient.Read(ActivityInstance.Assignee.IdRef, ReadOptions);
            isInitialized = true;
        }

        protected virtual void FinishActivity()
        {
            if (!isInitialized) Initialize();
            if (!IsSuspended)
            {
                CoreServiceClient.FinishActivity(ActivityInstance.Id, new ActivityFinishData()
                {
                    Message = FinishMessage ?? "Automatic Activity Finished",
                    NextAssignee = NextAssignee != null ? new LinkToTrusteeData() { IdRef = NextAssignee.Id } : null
                }, ReadOptions);
            }
        }

        protected override void Resume(string bookmark)
        {
            SuspendedActivity = (ActivityInstanceData)CoreServiceClient.Read(ActivityInstance.Id, ReadOptions);
        }

        private PublicationData GetPublication()
        {
            var publicationId = new TcmUri(WorkItems.ElementAt(0).Id).PublicationId;
            var publicationUri = String.Format("tcm:0-{0}-1", publicationId);

            return (PublicationData)CoreServiceClient.Read(publicationUri, ReadOptions);
        }

        protected void MarkWorkflowResult(TranslationWorkflowResults workflowResult, bool itemIsInWorkflow = true, bool updateWorkflowContextVariable = true)
        {
            // try to write to Workflow variables
            if (updateWorkflowContextVariable)
            {
                try
                {
                    var variableValue = string.Empty;
                    var variables = ProcessInstance.Variables;
                    if (variables.TryGetValue(WorkflowResultContextKey, out variableValue))
                    {
                        variables[WorkflowResultContextKey] = workflowResult.ToString();
                    }
                    else
                    {
                        variables.Add(WorkflowResultContextKey, workflowResult.ToString());
                    }
                }
                catch { }
            }

            var items = Bundle != null ? new[] { Bundle.Id } : WorkItems.Select(s => s.Subject.IdRef).ToArray();
            var result = new List<string>();
            foreach (var itemId in items)
            {
                var value = SaveApplicationData(itemId, WorkflowResultContextKey, workflowResult.ToString(), itemIsInWorkflow);
                result.Add(string.Format("[uri: {0}, ApplicationId: {1}, data: {2}]", itemId, WorkflowResultContextKey, value));
            }
            Helper.Log.Debug(string.Format("ApplicationData updated for items: {0}", string.Join(" ", result)), Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
        }
        private string SaveApplicationData(string itemUri, string applicationId, string value, bool itemIsInWorkflow = true)
        {
            var str = string.Empty;
            if (itemIsInWorkflow)
            {
                if (!itemUri.EndsWith("-v0")) itemUri = string.Format("{0}-v0", itemUri);
            }
            else
            {
                if (itemUri.EndsWith("-v0")) itemUri = itemUri.Remove(itemUri.Length - 3);
            }
            try
            {
                var appData = new ApplicationData
                {
                    ApplicationId = applicationId,
                    Data = new ASCIIEncoding().GetBytes(value)
                };
                CoreServiceClient.SaveApplicationData(itemUri, new[] { appData });
                // check
                str = new UTF8Encoding().GetString(CoreServiceClient.ReadApplicationData(itemUri, applicationId).Data);
                if (str != value)
                {
                    throw new Exception(string.Format("Unexpected retrieved ApplicationData value: '{0}' where expecting '{1}'", str, value));
                }
                Helper.Log.Debug(string.Format("Successfully saved Application Data (ApplicationId: '{0}', Value: '{1}') for item (uri: {2})", applicationId, value, itemUri), Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
            }
            catch (Exception ex)
            {
                Helper.Log.Warning(string.Format("Error while saving Application Data (ApplicationId: '{0}', Value: '{1}') for item (uri: {2})", applicationId, value, itemUri), ex, Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
            }
            return str;
        }

        private VirtualFolderData GetBundle()
        {
            var bundleWorkItem = WorkItems.FirstOrDefault(b => new TcmUri(b.Subject.IdRef).ItemType == ItemType.VirtualFolder);
            if (bundleWorkItem != null)
            {
                return (VirtualFolderData)CoreServiceClient.Read(bundleWorkItem.Subject.IdRef, ReadOptions);
            }
            return null;
        }

        public UserData GetLastManualActivityPerformer()
        {
            ActivityInstanceData lastManualActivity = GetLastManualActivity();
            return (UserData)CoreServiceClient.Read(lastManualActivity.Performers.Last().IdRef, ReadOptions);
        }

        public ActivityInstanceData GetLastManualActivity()
        {
            IEnumerable<ActivityInstanceData> activityInstances =
                ProcessInstance.Activities.OfType<ActivityInstanceData>().OrderByDescending(o => o.StartDate);

            return activityInstances.First(a =>
            {
                TridionActivityDefinitionData activityDefinition =
                    (TridionActivityDefinitionData)CoreServiceClient.Read(a.ActivityDefinition.IdRef, ReadOptions);
                return String.IsNullOrEmpty(activityDefinition.Script);
            });
        }

        public static void Information(string message, string identifier = componentId)
        {
            Helper.Log.Info(message, identifier);
        }

        public string GetActivityDetails()
        {
            if (!isInitialized) Initialize();
            var item = CoreServiceClient.Read(WorkItems.FirstOrDefault().Subject.IdRef, ReadOptions);
            var user = GetLastManualActivityPerformer();
            var items = WorkItems.Select(w => CoreServiceClient.Read(w.Subject.IdRef, ReadOptions));
            var itemsInfo = string.Concat(items.Select(i =>
            {
                var status = "";
                if (i is ComponentData) status = ((ComponentData)i).ApprovalStatus.Title;
                else if (i is PageData) status = ((PageData)i).ApprovalStatus.Title;
                else if (i is VirtualFolderData) status = ((VirtualFolderData)i).ApprovalStatus.Title;
                return string.Format("[uri: {0}, title: {1}] status: '{2}'", i.Id, i.Title, status);
            }));
            return string.Format("Activity -> (uri: {0}, title: {1})\nProcess Definition -> (uri: {2}, title: {3})\nItems -> {4}\nAssignee -> (uri: {5}, title: {6})\nPerformer Last Manual Activity -> (uri: {7}, title: {8})",
                        ActivityInstance.Id,
                        ActivityInstance.Title,
                        ProcessDefinition.Id,
                        ProcessDefinition.Title,
                        itemsInfo,
                        Assignee.Id,
                        Assignee.Description,
                        user.Id,
                        user.Description
                    );
        }

    }
}
