﻿namespace TDS17.Workflow.Activities
{
    class Approve : BaseActivity
    {
        private const string Message = "Approved by Automated Activity '{0}'";

        protected override void Initialize()
        {
            base.Initialize();
            FinishMessage = string.Format(Message, ActivityInstance.Title);
        }

        protected override void Execute()
        {
            Initialize();
            MarkWorkflowResult(TranslationWorkflowResults.Approved, true, false);
            FinishActivity();
            Helper.Log.Debug(string.Format("{0}\n\n{1}", FinishMessage, GetActivityDetails()), Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod()));
        }

    }
}
