﻿using Tridion.ContentManager.CoreService.Client;
using Tridion.ContentManager.CoreService.Workflow;

namespace TDS17.Workflow.Activities
{
    public class Test : ExternalActivity
    {
        protected override void Execute()
        {
            var message = string.Format("Activity automatically finished by Automated Activity (TEST TBB) '{0}'", ActivityInstance.Title);
            FinishActivity(message);
        }

        protected override void Expire()
        {
            var message = string.Format("Activity automatically finished by expiration script of Activity (TEST TBB) '{0}'", ActivityInstance.Title);
            FinishActivity(message);
        }

        protected void FinishActivity(string message)
        {
            CoreServiceClient.FinishActivity(ActivityInstance.Id, new ActivityFinishData()
            {
                Message = message ?? "Activity Finished automatically"
            }, null);
        }


    }
}
