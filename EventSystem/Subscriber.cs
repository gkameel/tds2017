﻿using Tridion.ContentManager.ContentManagement;
using Tridion.ContentManager.Extensibility.Events;
using Tridion.ContentManager.Extensibility;
using Tridion.ContentManager.CommunicationManagement;
using System.Collections.Generic;
using System;
using Tridion.ContentManager;
using Tridion.ContentManager.Workflow;
using TDS17.EventSystem.EventHandlers;
using TDS17.Workflow;

namespace TDS17.EventSystem
{
    [TcmExtension("TDS17EventSystem")]
    public class Subscriber : TcmExtension, IDisposable
    {
        private readonly List<EventSubscription> _subScriptions = new List<EventSubscription>();

        public Subscriber()
        {
            if (Helper.Config.EnableEventSystem)
            {
                Subscribe();
            }
        }

        public void Subscribe()
        {
            /** Translation & Workflow **/
            _subScriptions.Add(EventSystem.Subscribe<VersionedItem, GetProcessDefinitionEventArgs>(WorkflowEvents.OverruleProcessDefinition, EventPhases.Processed)); // Set Process Definition for Translation Workflow
            _subScriptions.Add(EventSystem.Subscribe<VersionedItem, AddToWorkflowEventArgs>(WorkflowEvents.LogWorkflowStart, EventPhases.TransactionCommitted)); // Log Workflow Start
            _subScriptions.Add(EventSystem.Subscribe<ActivityInstance, FinishActivityEventArgs>(WorkflowEvents.DoActivityFinishActions, EventPhases.TransactionCommitted)); // Perform finishing actions when workflow activity is finished
            _subScriptions.Add(EventSystem.Subscribe<Process, FinishProcessEventArgs>(WorkflowEvents.DoProcessFinishActions, EventPhases.TransactionCommitted)); // Perform finishing actions when translation workflow is finished
            _subScriptions.Add(EventSystem.Subscribe<IdentifiableObject, SaveApplicationDataEventArgs>(WorkflowEvents.LogAppData, EventPhases.Processed)); // Log Debug info when AppData is saved
            /** // Translation & Workflow **/
        }

        public void Dispose()
        {
            foreach (EventSubscription subscription in _subScriptions)
            {
                subscription.Unsubscribe();
            }
        }

    }
}
