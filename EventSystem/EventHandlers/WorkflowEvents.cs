﻿using TDS17.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tridion.ContentManager;
using Tridion.ContentManager.CommunicationManagement;
using Tridion.ContentManager.ContentManagement;
using Tridion.ContentManager.Extensibility;
using Tridion.ContentManager.Extensibility.Events;
using Tridion.ContentManager.Publishing;
using Tridion.ContentManager.Workflow;
using System.Xml.Linq;
using Tridion.ContentManager.ContentManagement.Fields;
using Tridion.ContentManager.Security;

namespace TDS17.EventSystem.EventHandlers
{
    class WorkflowEvents
    {
        private const string WorkflowResultContextKey = "WorkflowResult";
        private const string TranslationBundlesFolderWebDavPath = "/Building%20Blocks/Translation%20Bundles";
        private const string ReTranslateBundleTitle = "Re-Translate Bundle";

        internal static void OverruleProcessDefinition(VersionedItem item, GetProcessDefinitionEventArgs args, EventPhases phase)
        {
            {
                var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());

                // EXIT if not resolving process definition event
                if (!(args is GetProcessDefinitionEventArgs)) return;

                // EXIT if not correct event phase
                if (phase != EventPhases.Initiated && phase != EventPhases.Processed) return;

                var currentUserId = item.Session.User.Id.ToString();
                var processDefinition = ((GetProcessDefinitionEventArgs)args).ResolvedProcessDefinition;
                Helper.Log.Debug(string.Format("ResolvedProcessDefinition -> (uri: {5}, title: {6})\nIdentifiableObject -> (uri: {0}, title: {1}, type: {2}, isLocalized: {7})\nEventArgs -> (type: {3})\nPhase: {4}\n User -> (uri: {8}, Description: {9})", item.Id, item.Title, item.GetType(), args.GetType(), phase, (processDefinition == null) ? "" : processDefinition.Id, (processDefinition == null) ? "" : processDefinition.Title, item.IsLocalized, item.Session.User.Id, item.Session.User.Title), componentId);

                // EXIT if not WF user
                if (!Helper.Config.WorkflowTriggeringUserNames.Contains(item.Session.User.Title, StringComparer.OrdinalIgnoreCase)) return;

                // Set the process definition
                if (item.Id.ToString().ToLower().EndsWith("v0"))
                {
                    try
                    {
                        var defUri = Helper.Config.GetTcmUriFromConfig("WorkflowDefinitionId", item.Id.PublicationId, (int)ItemType.ProcessDefinition);
                        var def = (ProcessDefinition)item.Session.GetObject(defUri);
                        ((GetProcessDefinitionEventArgs)args).ResolvedProcessDefinition = def;
                    }
                    catch (Exception ex)
                    {
                        Helper.Log.Error(string.Format("ERROR while setting process definition for IdentifiableObject (uri: {0}, title: {1}, type: {2})", item.Id, item.Title, item.GetType()), ex, componentId);
                    }
                }

            }
        }

        internal static void LogWorkflowStart(RepositoryLocalObject item, AddToWorkflowEventArgs args, EventPhases phase)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            var processDefinition = args.ActivityInstance.ActivityDefinition.ProcessDefinition;
            var user = args.ActivityInstance.Assignee;
            Helper.Log.Info(string.Format("Tridion Workflow (uri: {2}, title: {3}) was started for item (uri: {0}, title: {1}) by user (uri: {4}, title: {5})", item.Id, item.Title, processDefinition.Id, processDefinition.Title, user.Id, user.Description), componentId);
        }

        internal static void DoActivityFinishActions(ActivityInstance activity, FinishActivityEventArgs args, EventPhases phase)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            Helper.Log.Debug(string.Format("Activity FINISHED\nItem -> (uri: {0}, title: {1}, type: {2})\nArgs -> {3}\nphase -> {4}", activity.Id, activity.Title, activity.GetType(), args.GetType(), phase.ToString()), componentId);

            // exit if configured Workflow
            var defTitle = "";
            try
            {
                var defUri = Helper.Config.GetTcmUriFromConfig("WorkflowDefinitionId", activity.Id.PublicationId, (int)ItemType.ProcessDefinition);
                var def = activity.Session.GetObject(defUri) as ProcessDefinition;
                if (def == null) throw new Exception(string.Format("ProcessDefinition '{0}' does not exist", defUri));
                defTitle = def.Title;
            }
            catch (Exception ex)
            {
                Helper.Log.Error(string.Format("ERROR while getting configured workflow process definition for activity (uri: {0}, title: {1}, type: {2})", activity.Id, activity.Title, activity.GetType()), ex, componentId);
            }
            if (activity.ActivityDefinition.ProcessDefinition.Title != defTitle) return;

            var variableValue = string.Empty;
            try
            {
                var processInstance = new ProcessInstance(activity.Process.Id, activity.Session);
                var workflowVariables = processInstance.Variables;
                if (!workflowVariables.TryGetValue(WorkflowResultContextKey, out variableValue)) variableValue = string.Empty;
            }
            catch { }
            Helper.Log.Debug(string.Format("Workflow variable at activity '{1}' -> {0}", variableValue, activity.Title), componentId);

            if (variableValue == TranslationWorkflowResults.Rejected.ToString() || variableValue == TranslationWorkflowResults.RejectedAndTranslationRequired.ToString())
            {
                // try and kill the workflow
                try
                {
                    activity.Process.Delete();
                    var itemInfo = string.Concat(activity.WorkItems.Select(w => w.Subject).Cast<IdentifiableObject>().Select(i => string.Format("[uri: {0}, title: {1}, type: {2}]", i.Id, i.Title, i.GetType())));
                    Helper.Log.Debug(string.Format("Workflow process terminated !\nActivity -> [uri: {0}, title: '{1}', process definition: '{2}']\nItems: -> {3}", activity.Id, activity.Title, activity.ActivityDefinition.ProcessDefinition.Title, itemInfo), componentId);
                }
                catch (Exception ex)
                {
                    Helper.Log.Error(string.Format("ERROR while trying to kill workflow process for activity (uri: {0}, title: {1}, type: {2})", activity.Id, activity.Title, activity.GetType()), ex, componentId);
                }
            }
        }

        internal static void DoProcessFinishActions(Process process, FinishProcessEventArgs args, EventPhases phase)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            var reason = args.Reason;
            var processHistory = args.ProcessHistory;
            var currentProcessDefinitionTitle = (processHistory != null) ? processHistory.ProcessDefinitionTitle : "";
            Helper.Log.Debug(string.Format("Process FINISH with reason '{5}'\nProcess -> (uri: {0}, title: {1}, type: {2}, definition: {6})\nEventArgs -> (type: {3})\nPhase: {4}", process.Id, process.Title, process.GetType(), args.GetType(), phase, reason, currentProcessDefinitionTitle), componentId);

            // exit if not Translation Workflow
            var defTitle = "";
            try
            {
                var defUri = Helper.Config.GetTcmUriFromConfig("WorkflowDefinitionId", process.Id.PublicationId, (int)ItemType.ProcessDefinition);
                var def = process.Session.GetObject(defUri) as ProcessDefinition;
                if (def == null) throw new Exception(string.Format("ProcessDefinition '{0}' does not exist", defUri));
                defTitle = def.Title;
                //Helper.LogDebug(string.Format("Configured Translation Process Definition Title -> '{0}'", defTitle), componentId);
            }
            catch (Exception ex)
            {
                Helper.Log.Error(string.Format("ERROR while getting translation workflow process definition for process (uri: {0}, title: {1}, type: {2})", process.Id, process.Title, process.GetType()), ex, componentId);
            }
            if (currentProcessDefinitionTitle != defTitle) return;

            var items = process.Subjects.Cast<IdentifiableObject>();
            var workflowResults = new Dictionary<string, Pair<IdentifiableObject, string>>();
            foreach (var item in items)
            {
                var appData = item.LoadApplicationData(WorkflowResultContextKey);
                if (appData != null && (!workflowResults.ContainsKey(item.Id)))
                {
                    workflowResults.Add(item.Id, new Pair<IdentifiableObject, string>(item, Encoding.ASCII.GetString(appData.Data)));
                }
            }

            var appdataInfo = workflowResults.Select((k) => string.Format("[uri: {0} (item.Id: {3}), application: '{1}', data: '{2}']", k.Key, WorkflowResultContextKey, k.Value.Second, k.Value.First.Id));
            Helper.Log.Debug(string.Format("Workflow Result AppData -> {0}", string.Concat(appdataInfo)), componentId);


            foreach (KeyValuePair<string, Pair<IdentifiableObject, string>> workflowResult in workflowResults)
            {
                // Perform POST WORKFLOW actions on workflow items

                try
                {
                    var item = workflowResult.Value.First;
                    var result = workflowResult.Value.Second;
                    if (reason == ProcessFinishReason.Terminated)
                    {
                        // Workflow is terminated: item has been rejected

                        // Publish to Staging 
                        var stagingTargetType = GetTargetType(Helper.Config.StagingTargetTypeTitle, process.Session);
                        //Publish(item, stagingTargetType, false, true); // disabled for TDS

                    }
                    else if (reason == ProcessFinishReason.CompletedNormally || reason == ProcessFinishReason.ForceFinished)
                    {
                        // Item has been approved
                        // do some stuff here...
                    }
                    else
                    {
                        // unexpected process end: warn

                        Helper.Log.Warning(string.Format("Unexpected process finish reason: '{0}'", reason), componentId);
                    }

                    Helper.Log.Info(string.Format("Successfully finished '{0}' workflow with result '{1}' and reason '{2}' for item (uri: {3}, title: {4}) ", currentProcessDefinitionTitle, result, reason, item.Id, item.Title), componentId);
                }
                catch (Exception ex)
                {
                    Helper.Log.Warning(string.Format("An error occured while processing item (uri: {0}, title: {1}) after workflow (reason: '{2}', result: '{3}')", workflowResult.Key, workflowResult.Value.First.Title, reason, workflowResult.Value.Second), ex, componentId);
                }
            }
        }

        internal static void LogAppData(IdentifiableObject item, SaveApplicationDataEventArgs args, EventPhases phase)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            var appdataInfo = string.Concat(args.ApplicationDataCollection.Select(a => string.Format("[uri: {0}, applicationId: '{1}', data: '{2}']", item.Id, a.ApplicationId, Encoding.ASCII.GetString(a.Data))));
            Helper.Log.Debug(string.Format("AppData saved ! -> {0}", appdataInfo), componentId);
        }

        private static void Publish(IdentifiableObject item, TargetType targetType, bool rollBackOnFailure = false, bool includeComponentLinks = false, PublishPriority priority = PublishPriority.Normal)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            try
            {
                IEnumerable<IdentifiableObject> items = new List<IdentifiableObject>() { item };
                IEnumerable<TargetType> targets = new List<TargetType>() { targetType };
                PublishInstruction instruction = new PublishInstruction(item.Session)
                {
                    DeployAt = DateTime.Now,
                    RenderInstruction = new RenderInstruction(item.Session)
                    {
                        RenderMode = RenderMode.Publish
                    },
                    ResolveInstruction = new ResolveInstruction(item.Session)
                    {
                        IncludeComponentLinks = includeComponentLinks
                    },
                    RollbackOnFailure = rollBackOnFailure,
                    StartAt = DateTime.MinValue
                };
                PublishEngine.Publish(items, instruction, targets);
                Helper.Log.Debug(String.Format("Succesfully published item (uri: {0}, title: {1}) to '{2}' (uri: {3})", item.Id, item.Title, targetType.Title, targetType.Id), componentId);
            }
            catch (Exception ex)
            {
                Helper.Log.Warning(string.Format("Could not publish item (uri: {0}, title: {1}) to '{2}'", item.Id, item.Title, targetType.Title), ex, componentId);
            }
        }
        private static void AddToRetranslateBundle(IdentifiableObject item)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            Bundle bundle = null;
            try
            {
                bundle = GetRetranslateBundle(item);
                bundle.AddItem((RepositoryLocalObject)item);
                bundle.Save();
                Helper.Log.Debug(string.Format("Succesfully added item (uri: {0}, title: '{1}', type: '{2}') to Re-Translate Bundle (uri: {3}, title: '{4}', type: '{5}', webdavurl: '{6}')", item.Id, item.Title, item.GetType(), (bundle != null) ? bundle.Id : "N/A", (bundle != null) ? bundle.Title : "N/A", (bundle != null) ? bundle.GetType().ToString() : "N/A", (bundle != null) ? bundle.WebDavUrl : "N/A"), componentId);
            }
            catch (Exception ex)
            {
                Helper.Log.Warning(string.Format("Error while adding item (uri: {0}, title: '{1}', type: '{2}') to Re-Translate Bundle (uri: {3}, title: '{4}', type: '{5}', webdavurl: '{6}')", item.Id, item.Title, item.GetType(), (bundle != null) ? bundle.Id : "N/A", (bundle != null) ? bundle.Title : "N/A", (bundle != null) ? bundle.GetType().ToString() : "N/A", (bundle != null) ? bundle.WebDavUrl : "N/A"), ex, componentId);
            }
        }
        private static Bundle GetRetranslateBundle(IdentifiableObject item)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            Bundle reTranslateBundle = null;
            try
            {
                var session = item.Session;
                var publication = (Repository)session.GetObject(item.Id.GetContextRepositoryUri());
                var webdavUrlBase = publication.WebDavUrl;
                var folderWebDavUrl = string.Format("{0}{1}", webdavUrlBase, TranslationBundlesFolderWebDavPath);
                var folder = (Folder)session.GetObject(folderWebDavUrl);
                // find existing ReTranslate bundle
                var elements = folder.GetListItems(
                    new OrganizationalItemItemsFilter(session)
                    {
                        ItemTypes = new[] { ItemType.VirtualFolder },
                        BaseColumns = ListBaseColumns.Extended
                    });
                var bundelIds = XElement.Parse(elements.OuterXml).Descendants()
                    .Where(e => e.Attribute(XName.Get("IsShared")).Value == "false")
                    .Where(e => e.Attribute(XName.Get("IsLocalized")).Value == "false")
                    .Where(e => e.Attribute(XName.Get("Title")).Value.StartsWith(ReTranslateBundleTitle))
                    .OrderBy(e => e.Attribute(XName.Get("Title")).Value)
                    .Reverse()
                    .Select(e => e.Attribute(XName.Get("ID")).Value);
                reTranslateBundle = (bundelIds.Any() ? (Bundle)session.GetObject(bundelIds.First()) : CreateRetranslateBundle(folder));
            }
            catch (Exception ex)
            {
                Helper.Log.Warning(string.Format("Error while retrieving Re-Translation Bundle for item (uri: {0},title: {1})", item.Id, item.Title), ex, componentId);
            }
            Helper.Log.Debug(string.Format("Found Re-Translation Bundle (uri: {3}, title: {4}, type: {5}) for item (uri: ({0}), title: {1}, type: {2})", item.Id, item.Title, item.GetType(), (reTranslateBundle != null) ? reTranslateBundle.Id : "", (reTranslateBundle != null) ? reTranslateBundle.Title : "", (reTranslateBundle != null) ? reTranslateBundle.GetType().ToString() : ""), componentId);
            return reTranslateBundle;
        }
        private static Bundle CreateRetranslateBundle(Folder organizationalItem)
        {
            var componentId = Helper.GetFullMethodName(System.Reflection.MethodBase.GetCurrentMethod());
            const string IncludeAlreadyTranslatedFieldName = "IncludeAlreadyTranslated";

            Bundle reTranslateBundle = null;
            try
            {
                var session = organizationalItem.Session;
                var title = string.Format("{0} {1:yy-MM-dd_HH-mm}", ReTranslateBundleTitle, DateTime.Now);
                var bundle = new Bundle(session, organizationalItem.Id)
                {
                    Title = title
                };
                var publication = (Repository)session.GetObject(organizationalItem.Id.GetContextRepositoryUri());
                var webDavUrl = string.Format("{0}{1}", publication.WebDavUrl, Helper.Config.ReTranslateBundleSchemaWebdavPath);
                var schema = session.GetObject(webDavUrl) as Schema;
                if (schema != null)
                {
                    bundle.MetadataSchema = schema;
                    var metaDataFields = new ItemFields(schema);
                    var fieldsInfo = string.Concat(metaDataFields.Select(f => string.Format("[name: '{0}', definition (name: '{1}', description: '{2}', type: {3})]", f.Name, f.Definition.Name, f.Definition.Description, f.Definition.GetType())));
                    Helper.Log.Debug(string.Format("Schema (uri: {0}, title: '{1}') has fields -> {2}", schema.Id, schema.Title, fieldsInfo), componentId);
                    // set value 'Include already translated items - Bereits übersetzte Elemente einschließen'
                    var field = metaDataFields.Where(f => f.Name == IncludeAlreadyTranslatedFieldName).FirstOrDefault();
                    var value = string.Empty;
                    if (field != null)
                    {
                        var definition = field.Definition;
                        if (definition is SingleLineTextFieldDefinition)
                        {
                            var entries = ((SingleLineTextFieldDefinition)definition).List.Entries;
                            if (entries != null && entries.Any())
                            {
                                value = entries.First();
                                ((TextField)field).Value = value;
                                Helper.Log.Debug(string.Format("Setting metaData field (name: {2}, value: '{3}') based on Schema (uri: {4}, title: '{5}') for Bundle (uri: {0}, title: '{1}')", bundle.Id, bundle.Title, field.Name, value, schema.Id, schema.Title), componentId);
                            }
                        }
                    }
                    bundle.Metadata = metaDataFields.ToXml();
                }
                bundle.Save();
                reTranslateBundle = bundle;
            }
            catch (Exception ex)
            {
                Helper.Log.Warning(string.Format("Error while creating Re-Translation Bundle in folder (uri: {0},title: {1})", organizationalItem.Id, organizationalItem.Title), ex, componentId);
            }
            Helper.Log.Debug(string.Format("Created Re-Translation Bundle (uri: {3}, title: {4}, type: {5}) in folder (uri: ({0}), title: {1}, type: {2})", organizationalItem.Id, organizationalItem.Title, organizationalItem.GetType(), (reTranslateBundle != null) ? reTranslateBundle.Id : "", (reTranslateBundle != null) ? reTranslateBundle.Title : "", (reTranslateBundle != null) ? reTranslateBundle.GetType().ToString() : ""), componentId);
            return reTranslateBundle;
        }
        private static IEnumerable<TargetType> targetTypes;
        private static IEnumerable<TargetType> GetAllTargetTypes(Session session)
        {
            if (targetTypes != null) return targetTypes;
            IEnumerable<TargetType> targets = new List<TargetType>();
            targets = session.SystemManager.GetTargetTypes();
            targetTypes = targets;
            return targets;
        }
        private static TargetType GetTargetType(string Title, Session session)
        {
            return GetAllTargetTypes(session).Where(p => p.Title.ToLowerInvariant() == Title.ToLowerInvariant()).FirstOrDefault();
        }
        private sealed class Pair<TFirst, TSecond>
            : IEquatable<Pair<TFirst, TSecond>>
        {
            private readonly TFirst first;
            private readonly TSecond second;

            public Pair(TFirst first, TSecond second)
            {
                this.first = first;
                this.second = second;
            }

            public TFirst First
            {
                get { return first; }
            }

            public TSecond Second
            {
                get { return second; }
            }

            public bool Equals(Pair<TFirst, TSecond> other)
            {
                if (other == null)
                {
                    return false;
                }
                return EqualityComparer<TFirst>.Default.Equals(this.First, other.First) &&
                       EqualityComparer<TSecond>.Default.Equals(this.Second, other.Second);
            }

            public override bool Equals(object o)
            {
                return Equals(o as Pair<TFirst, TSecond>);
            }

            public override int GetHashCode()
            {
                return EqualityComparer<TFirst>.Default.GetHashCode(first) * 37 +
                       EqualityComparer<TSecond>.Default.GetHashCode(second);
            }
        }

    }
}
