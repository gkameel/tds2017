﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDS17.Workflow
{
    public enum TranslationWorkflowResults
    {
        None = 0,
        Approved = 1,
        ApprovedAndPublishLive = 2,
        Rejected = 3,
        RejectedAndTranslationRequired = 4,
        Test = 5,
        Finished = 6
    }
}
